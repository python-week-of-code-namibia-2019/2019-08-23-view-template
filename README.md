# Python Week Of Code, Namibia 2019

To launch the Django web server,
run

```
python manage.py runserver
```

We will ignore

```
You have 14 unapplied migration(s). Your project may not work properly until you apply the migrations for app(s): auth, contenttypes, sessions.
Run 'python manage.py migrate' to apply them.
```

for now.

Open [http://localhost:8000/](http://localhost:8000/) in your web browser.

[blog/templates/blog/index.html](blog/templates/blog/index.html) has the source code of the page that you see in your web browser.

## Task for Instructor

1. Add

   ```
   <p>Today is {% now "jS \o\f F" %}.</p>
   ```

   to the `<body>` element.

## Tasks for Learners

1. Add the current time.
   For information regarding the format,
   check https://docs.djangoproject.com/en/2.2/ref/templates/builtins/#date.